<?php
/* cherez widget */
function true_igv_get_photos( $args = array() ) {
	$o = get_option('true_ig_options');
	$defaults = array('true_user' => '', 'true_tag' => '',
                'true_show' => '', 'true_infinite' => '', 'true_w' => 'auto', 'true_h' => '300px',
                'true_size' => 'medium', 'true_counts'=>'all');
    $args = wp_parse_args( $args, $defaults );
	$args['true_show'] = ( $args['true_show'] == 'on' ) ? 'false' : 'true';
	$args['true_infinite'] = ( $args['true_infinite'] == 'on' ) ? 'true' : '';
	$o['color1'] = ( isset($o['color1']) ) ? $o['color1'] : '#4090DB';
	$o['color2'] = ( isset($o['color2']) ) ? $o['color2'] : '#2D6599';
	$o['fancybox'] = ( isset($o['fancybox']) == 'on' ) ? 'true' : 'false';
	
	$videoparams = '';
	if( isset($o['fancyboxcontrols']) && $o['fancyboxcontrols'] == 'on' )
		$videoparams .= 'controls ';
	if( !isset($o['fancyboxmute']) )
		$videoparams .= 'muted ';
	if( !isset($o['fancyboxloop']) )
		$videoparams .= 'loop ';
	if( !isset($o['fancyboxautoplay']) )
		$videoparams .= 'autoplay ';
	
	$videoparams = ( $videoparams ) ? 'data-igw-videoparams="' . trim($videoparams) . '"' : '';
	
		
	
	if( is_numeric( $args['true_w'] ) && $args['true_w'] > 0 ) $args['true_w'] = $args['true_w'] . 'px';
	if( is_numeric( $args['true_h'] ) && $args['true_h'] > 0 ) $args['true_h'] = $args['true_h'] . 'px';
	
	$username = ( $args['true_user'] ) ? ' data-igw-username="' . mb_strtolower(ltrim($args['true_user'],'@')) . '"' : '';
	$hashtag = ( $args['true_tag'] ) ? ' data-igw-hashtag="' . mb_strtolower(str_replace("#","",$args['true_tag'])) . '"' : '';
	
	return '<div data-igw
		 data-igw-api="' . igw_get_plg_uri() . '/api/index.php"
         '.$username.$hashtag.'
         data-igw-show-heading="' . $args['true_show'] . '"
         data-igw-scroll="' . $args['true_infinite'] . '"
         data-igw-width="' . $args['true_w'] . '"
         data-igw-height="' . $args['true_h'] . '"
         data-igw-image-size="' . $args['true_size'] . '"
         data-igw-counts="' . $args['true_counts'] . '"
         data-igw-button="' . $o['color1'] . '"
         data-igw-lightbox="' . $o['fancybox'] .'" ' . $videoparams . '
         data-igw-buttonhover="' . $o['color2'] . '">
    </div>';

}
/* cherez redactor */
function bartag_func( $atts ) {
	
	if( isset($atts['user']) ) $args['true_user'] = $atts['user'];
	if( isset($atts['tag']) ) $args['true_tag'] = $atts['tag'];
	if( isset($atts['heading']) ) $args['true_show'] = ($atts['heading'] == 'yes') ? '' : 'on';
	if( isset($atts['scroll']) ) $args['true_infinite'] = ($atts['scroll'] == 'yes') ? 'on' : '';
	if( isset($atts['w']) ) $args['true_w'] = $atts['w'];
	if( isset($atts['h']) ) $args['true_h'] = $atts['h'];
	if( isset($atts['size']) ) $args['true_size'] = $atts['size'];
	if( isset($atts['counts']) ) $args['true_counts'] = $atts['counts'];
	
	return true_igv_get_photos( $args );
}
add_shortcode( 'true_instagram', 'bartag_func' );