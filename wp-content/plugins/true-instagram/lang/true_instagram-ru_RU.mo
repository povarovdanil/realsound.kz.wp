��    @        Y         �     �     �     �     �     �     �     �            I  $  x   n     �     �     �  /        E     T     f     r     �     �     �     �     �     �     �     �  
   	     	     )	  
   .	     9	  '   T	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     0
     5
  "   B
     e
  C   k
     �
     �
     �
  8   �
     
  	     	        '     5     7     9     >  =  D     �     �     �     �     �     �     �     	       +  /  �   [      [  3   |  &   �  H   �        '   >     f  (   {     �  9   �  &   �  $     I   B     �  &   �  /   �     �          0  #   =  !   a  _   �     �  +   �     "     1     @  /   Z     �     �  h   �       %        ;  #   N     r  #   �     �     �  C   �          9     U  K   q     �     �     �     �     	               .              "       %       3   -      	          5             ;                    &       $   0   /                 @   4       #   1   <   '   !   +       >   8              7       ,       9             
   6          =       .       )                ?   (   *          2          :                                         #Tag &larr; Previous 1,21...following 2,3,4,22,23...followers 2,3,4,22,23...following 2,3,4,22,23...posts 5,6,7...followers 5,6,7...following 5,6,7...posts <strong>Important information!</strong><br />Since Instagram API changes on 17th November 2015 the access token is required, otherwise the plugin will work until June 2016.<br />Please, <a href="options-general.php?page=instagam.php">continue to settings</a> and configure your access token, it takes just 2 minutes of your time. <strong>Instagram API error:</strong> cURL is not installed/enabled on your server. Please contact your hosting support. @User All profile media Allow infinite scroll An error occurred. See console for the details. Basic settings Check for updates Close (Esc) Counts behavior Default: Disable autoplay Do not loop videos Do not mute videos Filtered by given tag Follow Follow Button Color Follow Button Color:hover Full Width Get access token Hide Image Size Insert Instagram Shortcode Instagram photos by user or by hashtag. Instagram widget Instagram widget settings Large Medium Next &rarr; No users found. Off On Open full size images in popup Please select: Save Changes Settings Shortcode Parameters Show Show heading Show photos only, without heading. Small The requested content cannot be loaded.<br/>Please try again later. User @ Widget Height Widget Width You can specify only one of this: username or hashtag(s) follower followers following is not found. k m post posts Project-Id-Version: True Instagram Widget 1.4
POT-Creation-Date: 2016-08-20 16:25+0400
PO-Revision-Date: 2016-08-20 16:25+0400
Last-Translator: Misha Rudrastyh <true@truemisha.ru>
Language-Team: Misha Rudrastyh <true@truemisha.ru>
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.5
X-Poedit-Basepath: .
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c
X-Poedit-SearchPath-0: ../.
 #Тег &larr; Предыдущее подписка подписчика подписки публикации подписчиков подписок публикаций <strong>Важная информация!</strong><br />После изменений в API Instagram 17 ноября 2015 указывать access token  обязательно, если вы ещё этого не сделали, будьте готовы к тому, что плагин проработает до июня 2016.<br />Пожалуйста, <a href="options-general.php?page=instagam.php">перейдите в настройки</a> и задайте токен, это займёт две минуты вашего времени. <strong>Ошибка API Инстаграм:</strong> На вашем сервере не установлен/не включен cURL. Пожалуйста, свяжитесь со службой тех. поддержки вашего хостинга. @Имя пользователя Все фото/видео пользователя Загружать по скроллу Возникла ошибка. Подробности в консоли. Общие настройки Проверить обновления Закрыть (Esc) Что на счетчике медиа? По умолчанию Отключить автовоспроизведение Не зацикливать видео Включить звук видео Только отмеченные указанными хештегами Подписаться Цвет кнопки подписки Цвет кнопки при наведении Самый большой Получить токен Скрыть Размер изображений Галерея Инстаграм Фото из Инстаграм по имени пользователя или по тегу. Инстаграм Настройки виджета Instagram Большой Средний Следующее &rarr; Пользователей не найдено. Выкл Вкл Открывать полную версию изображения во всплывающем окне Выберите: Сохранить изменения Настройки Параметры шорткода Показать Показать заголовок Скрыть заголовок Маленький Произошла ошибка.<br/>Повторите позже. Пользователь @ Высота виджета Ширина виджета Укажите либо имя пользователя либо тег(и) подписчик подписчики подписки не найден. тыс. млн. публикация публикации 