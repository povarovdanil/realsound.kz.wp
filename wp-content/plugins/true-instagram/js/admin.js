jQuery(function($){
	$('input.color-field').wpColorPicker();
	if( $('#fancybox').is(":checked") )  {
		$('.fboxclass').slideDown(300);
	}
	$('#fancybox').change(function(){
		if( $(this).is(":checked") )  {
			$('.fboxclass').show();
		}else{
			$('.fboxclass').hide();
		}
	});
});
