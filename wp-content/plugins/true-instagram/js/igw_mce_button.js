(function() {
	tinymce.PluginManager.add('igw_mce_button', function( editor, url ) {
		editor.addButton( 'igw_mce_button', {
			icon: 'trueinsta',
			title: true_igw_object.param1,
			
							onclick: function() {
								editor.windowManager.open( {
									title: true_igw_object.param2,
									body: [
										/* yuzer */
										{
											id: 'tnmcebuttonbyval',
											type: 'textbox',
											name: 'textbox1Name',
											label: true_igw_object.text1,
											value: ''
										},
										/* heshteg */
										{
											type: 'textbox',
											name: 'textbox2Name',
											label: true_igw_object.text2,
											value: ''
										},
										/* zagolovok */
										{
											type: 'listbox',
											name: 'listbox1Name',
											label: true_igw_object.listbox1,
											'values': [
												{text: true_igw_object.listbox1_1, value: 'yes'},
												{text: true_igw_object.listbox1_2, value: 'no'}
											]
										},
										/* prokrutka */
										{
											type: 'listbox',
											name: 'listbox2Name',
											label: true_igw_object.listbox2,
											'values': [
												{text: true_igw_object.listbox2_1, value: 'yes'},
												{text: true_igw_object.listbox2_2, value: 'no'}
											]
										},
										/* shirina */
										{
											type: 'textbox',
											name: 'textbox3Name',
											label: true_igw_object.text3,
											value: ''
										},
										/* visota */
										{
											type: 'textbox',
											name: 'textbox4Name',
											label: true_igw_object.text4,
											value: ''
										},
										/* razmer */
										{
											id: 'tnmcebuttonby',
											type: 'listbox',
											name: 'listbox3Name',
											label: true_igw_object.listbox3,
											'values': [
												{text: true_igw_object.listbox3_1, value: 'small'},
												{text: true_igw_object.listbox3_2, value: 'medium'},
												{text: true_igw_object.listbox3_3, value: 'large'},
												{text: true_igw_object.listbox3_4, value: 'xlarge'}
											]
										},
										/* counts */
										{
											id: 'tnmcebuttoncount',
											type: 'listbox',
											name: 'listbox4Name',
											label: true_igw_object.listbox4,
											'values': [
												{text: true_igw_object.listbox4_1, value: 'all'},
												{text: true_igw_object.listbox4_2, value: 'filteredbytag'}
											]
										}
										
									],
									onsubmit: function( e ) { // это будет происходить после заполнения полей и нажатии кнопки отправки
										editor.insertContent( '[true_instagram user="' + e.data.textbox1Name + '" tag="' + e.data.textbox2Name + '" heading="' + e.data.listbox1Name + '" scroll="' + e.data.listbox2Name + '" w="' + e.data.textbox3Name + '" h="' + e.data.textbox4Name + '" size="' + e.data.listbox3Name + '" counts="' + e.data.listbox4Name + '"]');
									}
								});
							}
								
				
		});
	});
})();