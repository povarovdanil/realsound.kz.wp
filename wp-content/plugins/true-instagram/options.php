<?php
function true_ig_menu()
{
  add_options_page( __('Instagram widget', 'true_instagram'), __('Instagram widget', 'true_instagram'), 'manage_options', 'instagam.php', 'true_ig_page');  
}
add_action('admin_menu', 'true_ig_menu');


/**
 * Callback function to the add_theme_page
 * Will display the theme options page
 */ 
function true_ig_page()
{
?>
    <div class="wrap">
      <h2><?php _e('Instagram widget settings', 'true_instagram') ?></h2>
      <form method="post" enctype="multipart/form-data" action="options.php">
        <?php 
          settings_fields('true_ig_options'); 
          do_settings_sections('true_ig_options.php');
        ?>
            <p class="submit">  
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />  
            </p>  
            
      </form>
    </div>
    <?php
}


/**
 * Register the settings to use on the theme options page
 */
add_action( 'admin_init', 'true_ig_settings' );

/**
 * Function to register the settings
 */
function true_ig_settings()
{
    // Register the settings with Validation callback
    register_setting( 'true_ig_options', 'true_ig_options', 'igw_validate_settings' );

    // Add settings section
    add_settings_section( 'true_common_section', __('Basic settings', 'true_instagram'), '', 'true_ig_options.php' );

    // Create textbox field
    $field_args = array(
      'type'      => 'checkbox',
      'id'        => 'fancybox',
      'name'      => 'fancybox',
      'desc'      => __('Open full size images in popup', 'true_instagram'),
      'std'       => '',
      'label_for' => 'fancybox',
      'class'     => 'css_class'
    );
    add_settings_field( 'fancybox_checkbox', __('Fancybox', 'true_instagram'), 'true_ig_display_settings', 'true_ig_options.php', 'true_common_section', $field_args );
    
    $field_args = array(
      'type'      => 'checkbox',
      'id'        => 'fancyboxmute',
      'name'      => 'fancyboxmute',
      'desc'      => __('Do not mute videos', 'true_instagram'),
      'std'       => '',
      'label_for' => 'fancyboxmute',
      'class'     => 'css_class fboxclass'
    );
    add_settings_field( 'fancybox_mute', '', 'true_ig_display_settings', 'true_ig_options.php', 'true_common_section', $field_args );
    
    $field_args = array(
      'type'      => 'checkbox',
      'id'        => 'fancyboxloop',
      'name'      => 'fancyboxloop',
      'desc'      => __('Do not loop videos', 'true_instagram'),
      'std'       => '',
      'label_for' => 'fancyboxloop',
      'class'     => 'css_class fboxclass'
    );
    add_settings_field( 'fancybox_loop', '', 'true_ig_display_settings', 'true_ig_options.php', 'true_common_section', $field_args );
 
    $field_args = array(
      'type'      => 'checkbox',
      'id'        => 'fancyboxautoplay',
      'name'      => 'fancyboxautoplay',
      'desc'      => __('Disable autoplay', 'true_instagram'),
      'std'       => '',
      'label_for' => 'fancyboxautoplay',
      'class'     => 'css_class fboxclass'
    );
    add_settings_field( 'fancybox_autoplay', '', 'true_ig_display_settings', 'true_ig_options.php', 'true_common_section', $field_args );
    
    
   $field_args = array(
      'type'      => 'text',
      'id'        => 'color1',
      'name'      => 'color1',
      'desc'      => '',
      'std'       => '#4090DB',
      'label_for' => 'color1',
      'class'     => ' color-field'
    );
    add_settings_field( 'button_color', __('Follow Button Color', 'true_instagram'), 'true_ig_display_settings', 'true_ig_options.php', 'true_common_section', $field_args );
	
	$field_args = array(
      'type'      => 'text',
      'id'        => 'color2',
      'name'      => 'color2',
      'desc'      => '',
      'std'       => '#2D6599',
      'label_for' => 'color2',
      'class'     => ' color-field'
    );
    add_settings_field( 'button_color2', __('Follow Button Color:hover', 'true_instagram'), 'true_ig_display_settings', 'true_ig_options.php', 'true_common_section', $field_args );
    
    
     //add_settings_section( 'true_ig_section', __('Instagram APP settings', 'true_instagram'), '', 'true_ig_options.php' );
     
	/*$field_args = array(
      'type'      => 'text',
      'id'        => 'clientid',
      'name'      => 'clientid',
      'desc'      => __('<span style="background-color:#FEADAD;">Instagram Client ID is deprecated since 17th November 2015. Use access token instead.<br />If you still use this option, your widget will work until June 2016.</span>', 'true_instagram'),
      'std'       => '',
      'label_for' => 'clientid',
      'class'     => 'css_class'
    );
    add_settings_field( 'clientid_input', __('App ID', 'true_instagram'), 'true_ig_display_settings', 'true_ig_options.php', 'true_ig_section', $field_args );
    */
    
    /*$field_args = array(
      'type'      => 'token',
      'id'        => 'access_token',
      'name'      => 'access_token',
      'std'       => '',
      'desc'      => '',
      'label_for' => 'access_token',
      'class'     => ' css_class'
    );
    add_settings_field( 'atoken_input', __('Access token (required)', 'true_instagram'), 'true_ig_display_settings', 'true_ig_options.php', 'true_ig_section', $field_args );
*/
    
}


/**
 * Function to display the settings on the page
 * This is setup to be expandable by using a switch on the type variable.
 * In future you can add multiple types to be display from this function,
 * Such as checkboxes, select boxes, file upload boxes etc.
 */
function true_ig_display_settings($args)
{
    extract( $args );

    $option_name = 'true_ig_options';

    $options = get_option( $option_name );

    switch ( $type ) {  
          case 'text':
          	$std = isset($std) ? $std : '';
              if( isset($options[$id]) ){ 
					$options[$id] = stripslashes($options[$id]);  
					$options[$id] = esc_attr( $options[$id]);
			} else {
				$options[$id] = $std;
			}
              echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' />";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;
          case 'token':
          	$std = isset($std) ? $std : '';
              if( isset($options[$id]) ){ 
					$options[$id] = stripslashes($options[$id]);  
					$options[$id] = esc_attr( $options[$id]);
			} else {
				$options[$id] = $std;
			}
              echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' /> <a href='http://rudrastyh.com/tools/access-token' target='_blank' class='button-secondary'>" . __('Get access token', 'true_instagram') . "</a>";  
              echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";  
          break;
          case 'checkbox':
          	$checked = '';
          	if( isset($options[$id]) ){
          		if($options[$id] == 'on'){
          			$checked = " checked='checked'";
          		}
          	}
              echo "<label><input class='regular-text$class' type='checkbox' id='$id' name='" . $option_name . "[$id]' $checked /> ";  
              echo ($desc != '') ? $desc : "";
              echo "</label>";  
          break;
          
          case 'select': 
 			$select = "<select name='" . $option_name . "[$id]' id='" . $id . "'>";
 			foreach( $args as $name=>$val ): 
				$select .= '<option value="' . $val . '"';
				if( $options[$id] == $val )
					$select .= ' selected="selected"';
				$select .= '>' . $name . '</option>';
			endforeach;
			$select .= '</select>';
 			if( isset( $options['description'] ) )
				$select .= '<p class="description">' . $options['description'] . '</p>';
			echo $select;
		break;
 	
           
    }
}


/**
 * Callback function to the register_settings function will pass through an input variable
 * You can then validate the values and the return variable will be the values stored in the database.
 */
function igw_validate_settings($input)
{
  foreach($input as $k => $v)
  {
    $newinput[$k] = trim($v);
    
    // Check the input is a letter or a number
    if(!preg_match('/^[A-Z0-9 _]*$/i', $v)) {
      $newinput[$k] = '';
    }
  }

  return $input;
}