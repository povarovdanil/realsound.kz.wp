<?php
/*
Plugin Name: True Instagram
Plugin URI: http://rudrastyh.com/plugins/instagram-widget-shortcode
Description: Plugin allows you to create a widget with Instagram photos.
Version: 5.1
Text Domain: true_instagram
Author: Misha Rudrastyh
Author URI: http://rudrastyh.com

Copyright 2014-2015 Misha Rudrastyh ( http://rudrastyh.com )

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License (Version 2 - GPLv2) as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

define('TRUEIGW_VERSION','5.1');

/*
 * funkcii
 */
require_once plugin_dir_path( __FILE__ ) . '/functions.php';
/*
 * obnovlenie
 */
require_once plugin_dir_path( __FILE__ ) . '/class/class.upgrade.php';
/*
 * widghet
 */
require_once plugin_dir_path( __FILE__ ) . '/class/class.widget.php';
/*
 * nastroiki
 */
require_once plugin_dir_path( __FILE__ ) . '/options.php';


define( 'WP_IGPLG_DIR', WP_PLUGIN_DIR . '/' . basename( dirname( __FILE__ ) ) );
define( 'WP_IGPLG_URL', WP_PLUGIN_URL . '/' . basename( dirname( __FILE__ ) ) );
function igw_get_plg_uri() { return WP_IGPLG_URL; }


function igw_include_myscript() {
	$opts = get_option('true_ig_options');
	
	wp_enqueue_style( 'igwbutstyle', igw_get_plg_uri() . '/css/true_ig_widget.css', array(), TRUEIGW_VERSION );
	wp_enqueue_script('jquery');

	wp_enqueue_script( 'ig2', igw_get_plg_uri() . '/js/igw.js', array('jquery'), TRUEIGW_VERSION );
	$localize = array(
		'param1' => __( 'posts', 'true_instagram' ),
		'param2' => __( 'followers', 'true_instagram' ),
		'param3' => __( 'following', 'true_instagram' ),
		'param4' => __( 'Follow', 'true_instagram' ),
		'param5' => __( 'An error occurred. See console for the details.', 'true_instagram' ),
		'user' => __( 'User @', 'true_instagram' ),
		'notfound' => __( 'is not found.', 'true_instagram' ),
		'fancyprev' => __( '&larr; Previous', 'true_instagram'),
		'fancynext' => __( 'Next &rarr;', 'true_instagram'),
		'fancycls' => __( 'Close (Esc)', 'true_instagram'),
		'fancyerr' => __( 'The requested content cannot be loaded.<br/>Please try again later.', 'true_instagram'),
		'posts1' => __( 'post', 'true_instagram' ),
		'posts2' => _x( 'posts', '2,3,4,22,23...', 'true_instagram' ),
		'posts3' => _x( 'posts', '5,6,7...', 'true_instagram' ),
		'follr1' => __( 'follower', 'true_instagram' ),
		'follr2' => _x( 'followers', '2,3,4,22,23...', 'true_instagram' ),
		'follr3' => _x( 'followers', '5,6,7...', 'true_instagram' ),
		'follg1' => _x( 'following', '1,21...', 'true_instagram' ),
		'follg2' => _x( 'following', '2,3,4,22,23...', 'true_instagram' ),
		'follg3' => _x( 'following', '5,6,7...', 'true_instagram' ),
		'million' => __( 'm', 'true_instagram' ),
		'kilo' => __( 'k', 'true_instagram' ),
		
	);
	wp_localize_script( 'ig2', 'true_igw_object', $localize );
}
add_action( 'wp_enqueue_scripts', 'igw_include_myscript' );


/*adminka*/
function igw_include_admin_myscript() {
	/*mce ikonka*/
	wp_enqueue_style( 'wp-color-picker' ); 
	wp_enqueue_style('igwcss', igw_get_plg_uri() . '/css/true_ig_admin.css', array(), TRUEIGW_VERSION );
	
	wp_enqueue_script('igwjs', igw_get_plg_uri() . '/js/admin.js', array('jquery','wp-color-picker'), TRUEIGW_VERSION );
	/*localizacia*/
	$localize = array(
		'param1' => __( 'Insert Instagram Shortcode', 'true_instagram' ),
		'param2' => __( 'Shortcode Parameters', 'true_instagram' ),
		'text1' => __( '@User', 'true_instagram' ),
		'text2' => __( '#Tag', 'true_instagram' ),
		'listbox1' => __( 'Show heading', 'true_instagram' ),
		'listbox1_1' => __( 'Show', 'true_instagram' ),
		'listbox1_2' => __( 'Hide', 'true_instagram' ),
		'listbox2' => __( 'Allow infinite scroll', 'true_instagram' ),
		'listbox2_1' => __( 'On', 'true_instagram' ),
		'listbox2_2' => __( 'Off', 'true_instagram' ),
		'text3' => __( 'Widget Width', 'true_instagram' ),
		'text4' => __( 'Widget Height', 'true_instagram' ),
		'listbox3' => __( 'Image Size', 'true_instagram' ),
		'listbox3_1' => __( 'Small', 'true_instagram' ),
		'listbox3_2' => __( 'Medium', 'true_instagram' ),
		'listbox3_3' => __( 'Large', 'true_instagram' ),
		'listbox3_4' => __( 'Full Width', 'true_instagram' ),
		'listbox4' => __('Counts behavior', 'true_instagram'),
		'listbox4_1' => __('All profile media', 'true_instagram'),
		'listbox4_2' => __('Filtered by given tag', 'true_instagram')
		
	);
	wp_localize_script( 'igwjs', 'true_igw_object', $localize );
}
add_action( 'admin_enqueue_scripts', 'igw_include_admin_myscript' );

/*
 * inicializaciya knopki shortcoda 
 */
function igw_add_mce_button() {
	// проверяем права пользователя - может ли он редактировать посты и страницы
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return; // если не может, то и кнопка ему не понадобится, в этом случае выходим из функции
	}
	// проверяем, включен ли визуальный редактор у пользователя в настройках (если нет, то и кнопку подключать незачем)
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'igw_add_tinymce_script' );
		add_filter( 'mce_buttons', 'igw_register_mce_button' );
	}
}
add_action('admin_head', 'igw_add_mce_button');
 
// В этом функции указываем ссылку на JavaScript-файл кнопки
function igw_add_tinymce_script( $plugin_array ) {
	$plugin_array['igw_mce_button'] = igw_get_plg_uri() .'/js/igw_mce_button.js';
	return $plugin_array;
}
 
// Регистрируем кнопку в редакторе
function igw_register_mce_button( $buttons ) {
	array_push( $buttons, 'igw_mce_button' ); // идентификатор кнопки
	return $buttons;
}

/*
 * добавляем ссылку на настройки плагина
 */
function igw_plugin_action_links($links, $file) { 
	$settings_link = "<a href='options-general.php?page=instagam.php'>" . __('Settings') . "</a>";
	array_unshift( $links, $settings_link ); 
	return $links;
}
add_filter('plugin_action_links_' . plugin_basename( __FILE__ ), 'igw_plugin_action_links', 10, 2);    


$upd_server = ( get_bloginfo('language') == 'ru-RU' ) ? 'http://truemisha.ru/plg_api_upd/ins978e1_9781/info.json' : 'http://rudrastyh.com/plg_api_upd/ins978e1_9781/info.json';

$updater = new TrueUpdateChecker(
	$upd_server,
	__FILE__, 
	'true-instagram',
	'true_instagram'
);


function igw_admin_notice() {
   if( isset( $_GET['page'] ) && $_GET['page'] == 'instagam.php' )
   	return;
    echo '<div class="notice notice-warning is-dismissible">
     <p>' . __('<strong>Important information!</strong><br />Since Instagram API changes on 17th November 2015 the access token is required, otherwise the plugin will work until June 2016.<br />Please, <a href="options-general.php?page=instagam.php">continue to settings</a> and configure your access token, it takes just 2 minutes of your time.', 'true_instagram' ) . ' </p>
    </div>';
    	
}

//$o = get_option('true_ig_options');
//if( !$o['access_token'] )
//	add_action( 'admin_notices', 'igw_admin_notice' );





add_action( 'plugins_loaded', 'igw_load_plugin_textdomain' );
 
function igw_load_plugin_textdomain() {
	load_plugin_textdomain( 'true_instagram', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' ); 
}