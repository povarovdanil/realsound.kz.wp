<?php
if (!class_exists('true_instagram_widget')) {

class true_instagram_widget extends WP_Widget {
 
	/*
	 * создание виджета
	 */
	function __construct() {
		parent::__construct(
			'true_ig_widget', 
			__('Instagram widget', 'true_instagram'), 
			array( 'description' => __('Instagram photos by user or by hashtag.', 'true_instagram') )
		);
	}
 
	/*
	 * фронтэнд виджета
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
	
		if( isset( $args['before_widget'] ) ) 
			echo $args['before_widget'];
		
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		
		
		$args = array(
			'true_user' => $instance['true_user'],
			'true_tag' => $instance['true_tag'],
			'true_show' => $instance['true_show'],
			'true_infinite' => $instance['true_infinite'],
			'true_w' => $instance['true_w'],
			'true_h' => $instance['true_h'],
			'true_size' => $instance['true_size'],
			'true_counts' => $instance['true_counts']
		);
 		echo true_igv_get_photos( $args );
 		
 		if( isset( $args['after_widget'] ) ) 
			echo $args['after_widget'];
	}
 
	/*
	 * бакэнд виджета
	 */
	public function form( $instance ) {
		
		$fancybox='';
		$css='';
		$true_limit='';
		
	
		
		
		
		
		$title='';
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		$true_show='';
		if ( isset( $instance[ 'true_show' ] ) ) {
			$true_show = $instance[ 'true_show' ];
		}
		$true_tag='';
		if ( isset( $instance[ 'true_tag' ] ) ) {
			$true_tag = $instance[ 'true_tag' ];
		}
		$true_user='';
		if ( isset( $instance[ 'true_user' ] ) ) {
			$true_user = $instance[ 'true_user' ];
		}
		$true_infinite = '';
		if ( isset( $instance[ 'true_infinite' ] ) ) {
			$true_infinite = $instance[ 'true_infinite' ];
		}
		$true_w = '';
		if ( isset( $instance[ 'true_w' ] ) ) {
			$true_w = $instance[ 'true_w' ];
		}
		$true_h = '';
		if ( isset( $instance[ 'true_h' ] ) ) {
			$true_h = $instance[ 'true_h' ];
		}
		$true_size='medium';
		if ( isset( $instance[ 'true_size' ] ) ) {
			$true_size = $instance[ 'true_size' ];
		}
		$true_counts='all';
		if ( isset( $instance[ 'true_counts' ] ) ) {
			$true_counts = $instance[ 'true_counts' ];
		}
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:') ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'true_show' ); ?>" name="<?php echo $this->get_field_name( 'true_show' ); ?>"<?php echo ($true_show == 'on') ? ' checked="checked"' : '' ?>>
			<label for="<?php echo $this->get_field_id( 'true_show' ); ?>"><?php _e('Show photos only, without heading.', 'true_instagram') ?></label> 
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'true_user' ); ?>"><?php _e('@User', 'true_instagram') ?>:</label> 
			<input class="widefat true_check_user" id="<?php echo $this->get_field_id( 'true_user' ); ?>" name="<?php echo $this->get_field_name( 'true_user' ); ?>" type="text" value="<?php echo esc_attr( $true_user ); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'true_tag' ); ?>"><?php _e('#Tag', 'true_instagram') ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'true_tag' ); ?>" name="<?php echo $this->get_field_name( 'true_tag' ); ?>" type="text" value="<?php echo esc_attr( $true_tag ); ?>" placeholder="<?php _e('You can specify only one of this: username or hashtag(s)', 'true_instagram') ?>" />
		</p>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id( 'true_infinite' ); ?>" name="<?php echo $this->get_field_name( 'true_infinite' ); ?>"<?php echo ($true_infinite == 'on') ? ' checked="checked"' : '' ?>>
			<label for="<?php echo $this->get_field_id( 'true_infinite' ); ?>"><?php _e('Allow infinite scroll', 'true_instagram') ?></label> 
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_w' ); ?>"><?php _e('Widget Width', 'true_instagram') ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'true_w' ); ?>" placeholder="<?php _e('Default:', 'true_instagram') ?> auto." name="<?php echo $this->get_field_name( 'true_w' ); ?>" type="text" value="<?php echo esc_attr( $true_w ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_h' ); ?>"><?php _e('Widget Height', 'true_instagram') ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'true_h' ); ?>" placeholder="<?php _e('Default:', 'true_instagram') ?> 300px." name="<?php echo $this->get_field_name( 'true_h' ); ?>" type="text" value="<?php echo esc_attr( $true_h ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_size' ); ?>"><?php _e('Image Size', 'true_instagram') ?>:</label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'true_size' ); ?>" name="<?php echo $this->get_field_name( 'true_size' ); ?>">
				<option value="small"<?php if($true_size == 'small') echo ' selected="selected"' ?>><?php _e('Small', 'true_instagram') ?></option>
				<option value="medium"<?php if($true_size == 'medium') echo ' selected="selected"' ?>><?php _e('Medium', 'true_instagram') ?></option>
				<option value="large"<?php if($true_size == 'large') echo ' selected="selected"' ?>><?php _e('Large', 'true_instagram') ?></option>
				<option value="xlarge"<?php if($true_size == 'xlarge') echo ' selected="selected"' ?>><?php _e('Full Width', 'true_instagram') ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'true_counts' ); ?>"><?php _e('Counts behavior', 'true_instagram') ?>:</label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'true_counts' ); ?>" name="<?php echo $this->get_field_name( 'true_counts' ); ?>">
				<option value="all"<?php if($true_counts == 'all') echo ' selected="selected"' ?>><?php _e('All profile media', 'true_instagram') ?></option>
				<option value="filteredbytag"<?php if($true_counts == 'filteredbytag') echo ' selected="selected"' ?>><?php _e('Filtered by given tag', 'true_instagram') ?></option>
			</select>
		</p>
		<?php 
	}
 
	/*
	 * сохранение настроек виджета
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['true_show'] = ( ! empty( $new_instance['true_show'] ) ) ? $new_instance['true_show'] : '';
		$instance['true_user'] = ( ! empty( $new_instance['true_user'] ) ) ? strip_tags( trim( $new_instance['true_user'], '@' ) ) : '';
		$instance['true_tag'] = ( ! empty( $new_instance['true_tag'] ) ) ? strip_tags( trim( $new_instance['true_tag'], '#' ) ) : '';
		$instance['true_infinite'] = ( ! empty( $new_instance['true_infinite'] ) ) ? $new_instance['true_infinite'] : '';
		$instance['true_w'] = ( ! empty( $new_instance['true_w'] ) ) ? strip_tags( $new_instance['true_w'] ) : '';
		$instance['true_h'] = ( ! empty( $new_instance['true_h'] ) ) ? strip_tags( $new_instance['true_h'] ) : '';
		$instance['true_size'] = ( ! empty( $new_instance['true_size'] ) ) ? $new_instance['true_size'] : '';
		$instance['true_counts'] = ( ! empty( $new_instance['true_counts'] ) ) ? $new_instance['true_counts'] : '';
		return $instance;
	}
}
}

/*
 * регистрация виджета
 */
function true_load_widget() {
	register_widget( 'true_instagram_widget' );
}
add_action( 'widgets_init', 'true_load_widget' );



function true_return_ig_id(){
 	if( igw_check_curl() ) {
		$username = $_POST['username'];
		$client_array = array(
			'apiKey'      => 'ebc259a0540c497ba0f770cea329a0b9',
			'apiSecret'   => '60657b25105d47b898c9e3e7c21f3236',
			'apiCallback' => 'http://truemisha.ru'
		);
		$instagram = new Trueinstagram( $client_array );
		$usersearch = $instagram->searchUser($username, 20);
		if($usersearch->data){
			$i=0;
			_e('Please select:','true_instagram');
			foreach($usersearch->data as $user){
				$i++;
				?><label><input type="radio" class="true_r_select" name="user_id" value="<?php echo $user->id ?>" /> <img src="<?php echo $user->profile_picture ?>" width="24px" height="24px" /> <span><?php echo $user->username ?></span></label><?php
			}
		} else {
			_e('No users found.','true_instagram');
		}
	} else {
	 	_e('<strong>Instagram API error:</strong> cURL is not installed/enabled on your server. Please contact your hosting support.', 'true_instagram');
	}
	//print_r($usersearch);
	die();
}
 

add_action('wp_ajax_igid', 'true_return_ig_id');