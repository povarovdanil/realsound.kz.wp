<?php
/**
 * Created by PhpStorm.
 * User: danilpovarov
 * Date: 29.07.2018
 * Time: 20:27
 */
?>
<section class="cont">
    <div class="info" id="contacts">
        <div class="moduletable-mapinfo">
            <div class="custom-mapinfo">
                <p>
                    г.Алматы, Халиуллина 66<br>
                    Телефон: <span><?= MAIN_PHONE ?></span><br>
                    <?php if (BOTH_PHONE) { ?>
                    <span><?= MOBILE_PHONE ?></span><br>
                    <?php } ?>
                    E-mail: <a href="mailto:info@realsound.kz">info@realsound.kz</a>
                </p>
            </div>
        </div>
        <iframe width="600" height="350" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2904.406251256403!2d76.98009241563031!3d43.28481337913566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836fa6f3a9aebb%3A0x8c69cc9ff5296e28!2sReal+Sound!5e0!3m2!1sru!2skz!4v1532873659202"
                allowfullscreen></iframe>
    </div>
</section>
