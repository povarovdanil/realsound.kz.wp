$(document).on("click", ".send_post", function() {
    var a = $(this).closest("#form"),
        b = a.find("#exampleInputName").val(),
        c = a.find("#exampleInputPhone").val(),
        d = $(this).hasClass("nomodal");		
    $.ajax({
        url: "/mail.php",
        method: "POST",
        data: {
            username: b,
            phone: c
        },
        dataType: "json",
        success: function(b) {
            if ("ok" == b.status) {
				window.location.href = "https://realsound.kz/thanks";
            }
        }
    })
});