<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
<script src="/wp-content/themes/twentysixteen/js/libraries.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/jquery.parallax.js"></script>
<script src="/wp-content/themes/twentysixteen/js/script.js"></script>
<script src="/wp-content/themes/twentysixteen/js/forms.js"></script>
<svg xmlns="https://www.w3.org/2000/svg" version="1.1" id="displ">
  <filter id="blur">
    <feGaussianBlur stdDeviation="5" />
  </filter>

  <filter id="blurLogo">
    <feGaussianBlur stdDeviation="1" />
  </filter>
</svg>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
<script type="text/javascript" src="/wp-content/themes/twentysixteen/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/twentysixteen/js/slick.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/remodal.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/swiper.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/jquery.maskedinput.min.js"></script>
<script>
    new Swiper('.ratification-swiper', {
        slidesPerView: 3,
        slideToClickedSlide: true,
        mousewheelControl: true,
        centeredSlides: true,
        loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {
            840: {
                slidesPerView: 1,
            }
        }
    });
</script>
<script>
    $('.sliderr').slick({
  dots: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1
});
   $('.navigat_service').slick({
  dots: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1
});
</script>
<script>jQuery(function($){
   $("#phone, #exampleInputPhone").mask("+7(999) 999-9999");
   });
</script>
<script>
if(window.screen.width > 768) {
	document.write('<script src="/wp-content/themes/twentysixteen/js/script1.js"><\/script>');
}
</script>
<?php require_once 'counters.php'; ?>
</body>
</html>

