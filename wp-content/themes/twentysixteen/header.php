<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

require_once 'phoneHelper.php';

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
    <!--<script src="/wp-content/themes/twentysixteen/js/jquery.min.js"></script>-->

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/swiper.min.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/hover.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/animate.css"/>
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/css/remodal.css">
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/css/remodal-default-theme.css">
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/slick-theme.css"/>
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/lightbox.min.css">
    <script src="/wp-content/themes/twentysixteen/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/tooltip-curved.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/normalize.css"/>
    <link href="/wp-content/themes/twentysixteen/css/accordion-menu.css" rel="stylesheet"/>
    <link href="/wp-content/themes/twentysixteen/css/video.css" rel="stylesheet"/>

    <script src="/wp-content/themes/twentysixteen/js/alertify.min.js"></script>
    <style>





        /* entypo */
        [class*="entypo-"]:before {
            font-family: "entypo", sans-serif;
        }

        /* openwebicons */
        [class*="openwebicons-"]:before {
            font-family: "openwebicons", sans-serif;
        }

        /* zocial */
        [class*="zocial-"]:before {
            font-family: "zocial", sans-serif;
        }

        /* fontelico */
        [class*="fontelico-"]:before {
            font-family: "fontelico", sans-serif;
        }

        /* Fix OPW
        ----------------------------------------------------------*/
        /* openwebicons */
        [class*="openwebicons-"]:before {
            font-family: 'OpenWeb Icons', sans-serif;
        }

        /* Sidebar
        -----------------------------------------------------------------------------------------*/
        .menusign {
            display: block;
            width: 10px;
            line-height: .3;
            font-size: 2em;
            margin: .5em;
        }

        .menusign br {
            height: 0;
        }

        .sidebar {
            position: fixed;
            z-index: 9999999;
            left: 0;
            top: 0;
            height: 100%;
            width: 260px;
            background: #f3f3f3;
            -webkit-transition: left 0.3s ease, -webkit-box-shadow 0.3s ease 0.2s;
            transition: left 0.3s ease, box-shadow 0.3s ease 0.2s;
            -webkit-box-shadow: 0.5em 0 0 0 #1abc9c, 0.6em 0 0 0 #17a689;
            box-shadow: 0.5em 0 0 0 #1abc9c, 0.6em 0 0 0 #17a689;
        }

        .sidebar #close {
            float: right;
            margin: -5.5rem 1rem;
            font-size: 1.4em;
            color: #fa598d;
            text-align: right;
        }

        .sidebar.closed {
            left: -260px;
            -webkit-box-shadow: 0 0 0 #1abc9c;
            box-shadow: 0 0 0 #1abc9c;
        }

        .sidebar h2 {
            padding: .5em;
            color: #1abc9c;
        }

        .sidebar ul {
            padding: 0;
            margin: 0;
        }

        .sidebar ul li {
            width: 90%;
            margin: 5px 10px 5px 30px;
            float: left;
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 3px 6px;
            font-size: 14px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 2px solid #1abc9c;
            background: #1abc9c;
            color: #f3f3f3;
        }

        .sidebar ul li a {
            color: white;
        }

        .sidebar ul li:hover {
            background: #17a689;
            border-color: #17a689;
        }

        /* Blurred class

        This class is added to the container
        (#blurMe) when the sidebar is open.
        -----------------------------------*/
        .blurred {
            -webkit-transition: all 0.6s ease;
            transition: all 0.6s ease;
            -webkit-filter: blur(10px);
            filter: blur(10px);
            -webkit-filter: url(#blur);
            filter: url(#blur);
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='10');
        }

        /* Logo effect
        -----------------------------*/
        @-webkit-keyframes blur {
            66% {
                -webkit-filter: blur(1px);
                filter: blur(1px);
                -webkit-filter: url(#blurLogo);
                filter: url(#blurLogo);
                filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='1');
            }
        }

        @keyframes blur {
            66% {
                -webkit-filter: blur(1px);
                filter: blur(1px);
                -webkit-filter: url(#blurLogo);
                filter: url(#blurLogo);
                filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='1');
            }
        }
    </style>
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-147590-hCM0I';</script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1876895262571336'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1876895262571336&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1876895262571336', {
            em: 'insert_email_variable,'
        });
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1876895262571336&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>
<script>
    fbq('track', 'ViewContent', {
        value: 1.0,
        currency: 'USD'
    });
</script>

<body>
<div class="container">
    <div class="header fixed-header navbar-default navbar-fixed-top" role="navigation">
        <div class="line">
            <div id="blackline">
                <div id="cent">
                    <div class="col8">
                        <div class="col-md-10">
                            <div class="col2">
                                <div id="logotipp"><a href="/"><img
                                                src="/wp-content/themes/twentysixteen/images/logotipp.png"></a></div>
                            </div>
                            <div class="col4"><h3><span style="color:gold"><a href="/аренда">АРЕНДА</a><i> / </i><a
                                                href="/магазин-главная">ПРОДАЖА</a></span></h3></div>
                            <div class="col6"><!--<h2 id="tel" class="ringo-mob">+7 747 094 13 40</h2>-->
                                <h2 id="tel2"><?= MAIN_PHONE ?></h2></div>
                        </div>
                        <div class="col-md-2"><a href="#modal"><input type="submit" class="btn btn-info"
                                                                      value="ОСТАВИТЬ ЗАЯВКУ"></a></div>
                    </div>
                </div>
                <div id="soc">
                    <div class="col-md-3">
                        <a href="mailto:info@realsound.kz"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"
                                                              id="git"></i></a>
                        <a href="https://www.instagram.com/realsoundkz/" target="_blank"><i
                                    class="fa fa-instagram fa-lg" aria-hidden="true" id="git"></i></a>
                        <a href="https://www.facebook.com/REALSOUNDKZ/" target="_blank"><i
                                    class="fa fa-facebook-square fa-lg" aria-hidden="true" id="git"></i></a>
                        <a href="https://vk.com/club107718739" target="_blank"><i class="fa fa-vk fa-lg"
                                                                                  aria-hidden="true" id="git"></i></a>
                        <a href="https://www.youtube.com/channel/UCI1jwpjBlCD4_qFLH9Go46A" target="_blank"><i
                                    class="fa fa-youtube fa-lg" aria-hidden="true" id="git"></i></a></div>
                </div>
                <ul class="nav nav-pills pull-right">
                    <li><a href="#" id="trigger"><span class="entypo-menu"></span></a></li>
                </ul>
            </div>
        </div>

    </div>
    <div></div>
    <div class="sidebar closed">
        <header>
            <a href="#" id="close"><span class="entypo-cancel"></span></a>
        </header>
        <ul>
            <li><a href="index.php">Главная</a></li>
            <li><a href="index.php/о-нас">О нас</a></li>
            <li><a href="index.php/аренда">Аренда</a></li>
            <li><a href="index.php/магазин-главная">Продажа</a></li>
            <li><a href="index.php/услуги">Услуги</a></li>
            <li><a href="#gallery">Галерея</a></li>
            <!--<li><a href="#">Готовые</br>решения</a></li>-->
            <li><a href="#contacts">Контакты</a></li>
        </ul>
    </div>


</div>
<div id="blurrMe">
