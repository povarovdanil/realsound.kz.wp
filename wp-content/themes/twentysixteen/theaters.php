<?php
/*
Template Name: Theater
*/
require_once 'phoneHelper.php';
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/animate.css"/>
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/css/remodal.css">
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/css/remodal-default-theme.css">

    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/bootstrap/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/wp-content/themes/twentysixteen/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/tooltip-curved.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/normalize.css"/>
    <link rel="stylesheet" href="/wp-content/themes/twentysixteen/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/twentysixteen/css/semantic.min.css">
    <script src="/wp-content/themes/twentysixteen/js/alertify.min.js"></script>
    <style>





        /* entypo */
        [class*="entypo-"]:before {
            font-family: "entypo", sans-serif;
        }

        /* openwebicons */
        [class*="openwebicons-"]:before {
            font-family: "openwebicons", sans-serif;
        }

        /* zocial */
        [class*="zocial-"]:before {
            font-family: "zocial", sans-serif;
        }

        /* fontelico */
        [class*="fontelico-"]:before {
            font-family: "fontelico", sans-serif;
        }

        /* Fix OPW
        ----------------------------------------------------------*/
        /* openwebicons */
        [class*="openwebicons-"]:before {
            font-family: 'OpenWeb Icons', sans-serif;
        }

        /* Sidebar
        -----------------------------------------------------------------------------------------*/
        .menusign {
            display: block;
            width: 10px;
            line-height: .3;
            font-size: 2em;
            margin: .5em;
        }

        .menusign br {
            height: 0;
        }

        .sidebar {
            position: fixed;
            z-index: 9999999;
            left: 0;
            top: 0;
            height: 100%;
            width: 260px;
            background: #f3f3f3;
            -webkit-transition: left 0.3s ease, -webkit-box-shadow 0.3s ease 0.2s;
            transition: left 0.3s ease, box-shadow 0.3s ease 0.2s;
            -webkit-box-shadow: 0.5em 0 0 0 #1abc9c, 0.6em 0 0 0 #17a689;
            box-shadow: 0.5em 0 0 0 #1abc9c, 0.6em 0 0 0 #17a689;
        }

        .sidebar #close {
            float: right;
            margin: -5.5rem 1rem;
            font-size: 1.4em;
            color: #fa598d;
            text-align: right;
        }

        .sidebar.closed {
            left: -260px;
            -webkit-box-shadow: 0 0 0 #1abc9c;
            box-shadow: 0 0 0 #1abc9c;
        }

        .sidebar h2 {
            padding: .5em;
            color: #1abc9c;
        }

        .sidebar ul {
            padding: 0;
            margin: 0;
        }

        .sidebar ul li {
            width: 90%;
            margin: 5px 10px 5px 30px;
            float: left;
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 3px 6px;
            font-size: 14px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 2px solid #1abc9c;
            background: #1abc9c;
            color: #f3f3f3;
        }

        .sidebar ul li a {
            color: white;
        }

        .sidebar ul li:hover {
            background: #17a689;
            border-color: #17a689;
        }

        /* Blurred class

        This class is added to the container
        (#blurMe) when the sidebar is open.
        -----------------------------------*/
        .blurred {
            -webkit-transition: all 0.6s ease;
            transition: all 0.6s ease;
            -webkit-filter: blur(10px);
            filter: blur(10px);
            -webkit-filter: url(#blur);
            filter: url(#blur);
            filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='10');
        }

        /* Logo effect
        -----------------------------*/
        @-webkit-keyframes blur {
            66% {
                -webkit-filter: blur(1px);
                filter: blur(1px);
                -webkit-filter: url(#blurLogo);
                filter: url(#blurLogo);
                filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='1');
            }
        }

        @keyframes blur {
            66% {
                -webkit-filter: blur(1px);
                filter: blur(1px);
                -webkit-filter: url(#blurLogo);
                filter: url(#blurLogo);
                filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='1');
            }
        }
    </style>
    <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-147590-hCM0I';</script>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1876895262571336'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1876895262571336&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1876895262571336', {
            em: 'insert_email_variable,'
        });
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1876895262571336&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>
<script>
    fbq('track', 'ViewContent', {
        value: 1.0,
        currency: 'USD'
    });
</script>

<body>
<div class="container">
    <div class="header fixed-header navbar-default navbar-fixed-top" role="navigation">
        <div class="line">
            <div id="blackline">
                <div id="cent">
                    <div class="col8">
                        <div class="col-md-10">
                            <div class="col2">
                                <div id="logotipp"><a href="/"><img
                                                src="/wp-content/themes/twentysixteen/images/logotipp.png"></a></div>
                            </div>
                            <div class="col4"><h3><span style="color:gold"><a href="/аренда">АРЕНДА</a><i> / </i>
<a href="/магазин-главная">ПРОДАЖА</a></span></h3></div>
                            <div class="col6"><!--<h2 id="tel" class="ringo-mob">+7 747 094 13 40</h2>-->
                                <h2 id="tel2" class=""><?= MAIN_PHONE ?></h2></div>
                        </div>
                        <div class="col-md-2"><a href="#modal"><input type="submit" class="btn btn-info"
                                                                      value="ОСТАВИТЬ ЗАЯВКУ"></a></div>
                    </div>
                </div>
                <div id="soc">
                    <div class="col-md-3">
                        <a href="mailto:info@realsound.kz"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"
                                                              id="git"></i></a>
                        <a href="https://www.instagram.com/realsoundkz/" target="_blank"><i
                                    class="fa fa-instagram fa-lg" aria-hidden="true" id="git"></i></a>
                        <a href="https://www.facebook.com/REALSOUNDKZ/" target="_blank"><i
                                    class="fa fa-facebook-square fa-lg" aria-hidden="true" id="git"></i></a>
                        <a href="https://vk.com/club107718739" target="_blank"><i class="fa fa-vk fa-lg"
                                                                                  aria-hidden="true" id="git"></i></a>
                        <a href="https://www.youtube.com/channel/UCI1jwpjBlCD4_qFLH9Go46A" target="_blank"><i
                                    class="fa fa-youtube fa-lg" aria-hidden="true" id="git"></i></a></div>
                </div>
                <ul class="nav nav-pills pull-right">
                    <li><a href="#" id="trigger"><span class="entypo-menu"></span></a></li>
                </ul>
            </div>
        </div>


    </div>

    <div class="sidebar closed">
        <header>
            <a href="#" id="close"><span class="entypo-cancel"></span></a>
        </header>
        <ul>
            <li><a href="/">Главная</a></li>
            <li><a href="/о-нас">О нас</a></li>
            <li><a href="/аренда">Аренда</a></li>
            <li><a href="/магазин-главная">Продажа</a></li>
            <li><a href="/услуги">Услуги</a></li>
            <li><a href="index.php/#gallery">Галерея</a></li>
            <!--<li><a href="#">Готовые</br>решения</a></li>-->
            <li><a href="index.php/#contacts">Контакты</a></li>
        </ul>
    </div>
</div>
<div id="blurrMe">

    <section class="zayavka">
        <div class="container-fluid">
            <div class="borders1">
                <h1 id="bbw" style="color:gold">ЗВУК/СВЕТ</h1>
                <h1 style="color:white">ДЛЯ ТЕАТРОВ И КИНО</h1>
                <a href="#modal"><input type="submit" class="btn btn-info" value="ОСТАВИТЬ ЗАЯВКУ"></a>
                <div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title"
                     aria-describedby="modal1Desc">
                    <div class="remodalBorder">
                        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                        <div id="form"><h2 id="modal1Title">Оставьте ваши контактные данные и наш консультант свяжется с
                                вами</h2> <input type="text" class="putName" name="username" placeholder="Ваше имя"
                                                 id="exampleInputName" required> <input name="phone"
                                                                                        id="exampleInputPhone"
                                                                                        type="autofocus"
                                                                                        class="putPhone"
                                                                                        placeholder="Ваш номер телефона"
                                                                                        required> <input type="submit"
                                                                                                         name="submit"
                                                                                                         class="send_post"
                                                                                                         id="btnw"
                                                                                                         value="ОТПРАВИТЬ">
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="vybor">
        <div class="borders1">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" id="ss">
                    <div class="col-md-4" id="picc10"></div>
                    <div class="col-md-8"><h1 style="color:black; text-align:left">ПРОЕКЦИОННОЕ ОБОРУДОВАНИЕ</h1>
                    </div>
                </div>
                <div class="col-md-6 wow fadeInRight" id="ss1">
                    <div class="col-md-4" id="picc2"></div>
                    <div class="col-md-8"><h1 style="color:black; text-align:left">ТЕАТРАЛЬНЫЙ СВЕТ</h1></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-offset="250" id="ss">
                    <div class="col-md-4" id="picc5"></div>
                    <div class="col-md-8"><h1 style="color:black; text-align:left">СПЕЦЭФФЕКТЫ ДЛЯ СЪЕМОК КЛИПОВ</h1>
                    </div>
                </div>
                <div class="col-md-6 wow fadeInRight" data-wow-offset="200" id="ss1">
                    <div class="col-md-4" id="picc1"></div>
                    <div class="col-md-8"><h1 style="color:black; text-align:left">ЗВУКОВОЕ ОБОРУДОВАНИЕ</h1></div>
                </div>
            </div>

        </div>
    </section>
    <section class="complex">
        <div class="container-fluid">
            <div class="borders1">
                <div class="row" id="marg1"><p style="color:black;">Для Вашего удобства мы составили список самых
                        популярных комплектов для всех заведений. </br>
                        Но помните, что мы за индивидуальный подход, и готовы составить проект индивидуально под Ваше
                        заведение. </p></div>
                <div class="row" id="moshka">
                    <div class="col-md-4">
                        <ul id="stacked-menu">
                            <li id="retro">
                                <a href="#item1">
                                    <div class="arroww"></div>
                                    Клубы и рестораны</a>
                                <ul id="item1">
                                    <li id="first"><a href="#fir">Комплект проф. караоке</a></li>
                                    <li id="second"><a href="#sec">Оборудование для клуба</a></li>
                                    <li id="third"><a href="#thi">Комплект диджея</a></li>
                                    <li id="fourth"><a href="#four">Комплект звука для клуба</a></li>
                                </ul>
                            </li>
                            <li id="retro">
                                <a href="#item2">
                                    <div class="arroww"></div>
                                    Гостиницы и дома отдыха</a>
                                <ul id="item2">
                                    <li id="fifth"><a href="#fif">Комплект для озвучивания помещений, корпусная
                                            акустика</a></li>
                                    <li id="sixth"><a href="#six">Комплект для озвучивания помещений, потолочная
                                            акустика</a></li>
                                </ul>
                            </li>
                            <li id="retro"><a href="#item3">
                                    <div class="arroww"></div>
                                    Учебные заведения</a>
                                <ul id="item3">
                                    <li id="seventh"><a href="#sev">Звук для актового зала</a></li>
                                    <li id="eighth"><a href="#ei">Комплект звука для большого актового зала</a></li>
                                    <li id="ninth"><a href="#nin">Мобильный комплект усиления звука</a></li>
                                    <li id="tenth"><a href="#ten">Свет для Актового зала, большой комплект</a></li>
                                    <li id="eleventh"><a href="#ele">Свет для Актового зала, малый комплект</a></li>
                                </ul>
                            </li>
                            <li id="retro"><a href="#item4">
                                    <div class="arroww"></div>
                                    Бизнес Центры</a>
                                <ul id="item4">
                                    <li id="twelvth"><a href="#tw">Конференц-система для не больших переговорных</a>
                                    </li>
                                    <li id="thirteen"><a href="#th">Портативный комплект для не больших конференций</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="retro"><a href="#item5">
                                    <div class="arroww"></div>
                                    Культурные объекты</a>
                                <ul id="item5">
                                    <li id="twone"><a href="#two">Мобильный комплект усиления звука</a></li>
                                    <li id="twtwo"><a href="#twt">Озвучивание мечети</a></li>
                                    <li id="twthree"><a href="#twth">Озвучивание помещений</a></li>
                                </ul>
                            </li>
                            <li id="retro"><a href="#item6">
                                    <div class="arroww"></div>
                                    Театры и Кино</a>
                                <ul id="item6">
                                    <li id="twfour"><a href="#twf">Постановочное Световое Оборудование</a></li>
                                    <li id="twfive"><a href="#twfi">Профессиональный звуковой комплект для театра</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="retro"><a href="#item7">
                                    <div class="arroww"></div>
                                    ТРЦ</a>
                                <ul id="item7">
                                    <li id="twsix"><a href="#twsi">Комплект звукового оборудования Премиум класса</a>
                                    </li>
                                    <li id="twseven"><a href="#twse">Мобильный комплект звукового оборудования</a></li>
                                </ul>
                            </li>
                            <li id="retro"><a href="#item8">
                                    <div class="arroww"></div>
                                    Концерты</a>
                                <ul id="item8">
                                    <li id="tweight"><a href="#tweig">Концертная площадка до 500 человек</a></li>
                                    <li id="twnine"><a href="#twnin">Озвучивание крупных концертных площадок</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-8" id="pict1">
                        <div id="titgold"><h3>Комплект проф. караоке</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Караоке-машина AST-100 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активная акустическая система Electro-Voise ZLX15P — 2 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт Behringer QX1832 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микрофонная стойка типа "журавль" Soundking DD007B 2шт</span>
                                </div>
                                <div class="tooltip tooltip-west">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Вокальная радиосистема с 2-мя ручными передатчиками SHURE BLX288E/SM58 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая стойка Soundking DB009B</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict2">
                        <div id="titgold"><h3>Оборудование для клуба</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pro1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система Electro-Voice TX1152 — 4 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pro2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Компактный сабвуфер Electro-Voice TX1181 — 4 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pro3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт allen ZED22FX</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pro4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности Electro-Voice Q1212 - 2шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pro5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">DSP процессор для акустических систем Elecnhj-Voice DC-One</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pro6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Радиосистема с двумя ручными передатчиками SHURE BLX288E/PG58 — 1 шт.</span>
                                </div>

                                <div class="tooltip tooltip-west" id="pro8">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор Free Color Led Par 54  — 8 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pro9">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным движением ProLux Beam S230 — 4 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pro10">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным движением Free Color Wash W3610 - 6шт</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict3">
                        <div id="titgold"><h3>Комплект диджея</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="prr1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">DJ Проигрыватель PIONEER CDJ-900NXS — 2 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="prr2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Наушники PIONEER HDJ-1000-K наушники для DJ — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="prr3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">DJ-микшер PIONEER DJM-900NXS — 1 шт</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict4">
                        <div id="titgold"><h3>Комплект звука для клуба</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pr1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Soundking DB023 соединительная стойка для акустических систем — 2 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pr2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активный сабвуфер Electro-Voice EKX18P — 2 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pr3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Behringer xr18, 18 канальный цифровой микшерный пульт с управлением через планшет или ПК</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pr4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Радиосистема вокальная SHURE BLX24E/PG58 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pr5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Универсальная DJ  Установка PIONEER XDJ-R1</span>
                                </div>

                                <div class="tooltip tooltip-west" id="pr7">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система Electro-Voice EKX15P — 2 шт.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict5">
                        <div id="titgold"><h3>Комплект для озвучивания помещений, корпусная акустика</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcom1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности 100v 60W DSPPA MP200P - 1шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcom2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Динамический микрофон на подставке DSPPA EC200 - 1шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcom3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Потолочная акустическая система DSPPA dsp803 - 10шт</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict6">
                        <div id="titgold"><h3>Комплект для озвучивания помещений, корпусная акустика</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcon1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности со 100v 300W со встроенным MP3 проигрывателем и радио тюнером DSPPA MP1010U</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcon2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система с динамиками 2х4 Electro-Voice EVID4.2TB - 10шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcon3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Динамический микрофон на подставке DSPPA EC200 - 1шт
</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict7">
                        <div id="titgold"><h3>Звук для актового зала</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcor1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система Behringer B1520Pro — 2 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcor2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности Behringer EP2000</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcor3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт Behringer X1222 — 1 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcor4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микрофонная стойка типу "Журавль" Soundking DD007B 2шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcor5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Вокальная радиосистема с 2-мя ручными передатчиками SHURE BLX288E/PG58 — 1 шт.
</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict8">
                        <div id="titgold"><h3>Комплект звука для большого актового зала</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcoj1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности EP2000 - 2шт.</span>
                                </div>

                                <div class="tooltip tooltip-east" id="pcoj3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система Behringer b2520Pro - 2шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoj4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">3Way кросовер Behringer CX3400 1шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoj5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт Behringer QX1832USB - 1шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoj6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Вокальная радиосистема с 2-мя ручными передатчиками SHURE BLX288E/PG58 — 1 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoj7">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микрофонная стойка типу "Журавль" Soundking DD007B 2шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoj8">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активная акустическая система "Напольный Монитор" Behringer F1220D - 2шт
</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict9">
                        <div id="titgold"><h3>Мобильный комплект усиления звука</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcor1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Компактная 2000 Вт, 8-канальная портативная система, выполнена в виде чемодана Behringer PPA2000 - 1шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcor2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Вокальная радиосистема с 2-мя ручными передатчиками BLX288E/PG58 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcor3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">стойка для акустических систем черная Soundking DB009B — 2 шт.
</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict10">
                        <div id="titgold"><h3>Свет для Актового зала, большой комплект</h3></div>
                        <div id="prgold"><h1>110 000 тг.</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcob1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным двишением Spot CHAUVET IntimSpot LED 350 — 4 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcob2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор Chauvet SLIM PAR-64RGBA -  12 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcob3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным движением Free Color Wash W3610 - 6шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcob4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с проекционным эффектом CHAUVET Circus 2.0 IRC — 2 шт.
</span>
                                </div>

                                <div class="tooltip tooltip-east" id="pcob6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Оптический сплиттер DMX на 4 канала CHAUVET Data Stream 4 — 1 шт.
</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict11">
                        <div id="titgold"><h3>Свет для Актового зала, малый комплект</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcod1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор Free Color Led Par 54  — 8 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcod2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным движением Free Color Wash W3610 - 4шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcod3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным движением ProLux Beam S230 — 2 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcod4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Контроллер для управления световыми приборами avolites titan one - 1шт

</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict12">
                        <div id="titgold"><h3>Конференц-система для не больших переговорных</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcov1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности 100v 250w, DSPPA MP610</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcov2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система с настенным креплением, цвет белый HK-Audio IL60TW - 8шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcov3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт allen & heath ZED12FX
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcov4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Конденсаторный микрофон на гибком креплении SHURE MX418DC - 6шт

</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict13">
                        <div id="titgold"><h3>Портативный комплект для не больших конференций</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcos1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активная акустическая система с динамиком 1х8, Electro-Voice ZXA1 - 2шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcos2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Радиосистема с двумя ручными передатчиками SHURE BLX288E/PG58 1шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcos3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Миниатюрный микшерный пульт allen & heath zed6fx - 1шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcos4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микрофонная стойка типа "Журавль" Soundking DD007B - 2шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcos5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая стойка, цвет черный Soundking DB009B - 2шт

</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict14">
                        <div id="titgold"><h3>Мобильный комплект усиления звука</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcou1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Компактная 2000 Вт, 8-канальная портативная система, выполнена в виде чемодана Behringer PPA2000 - 1шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcou2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Вокальная радиосистема с 2-мя ручными передатчиками BLX288E/PG58 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcou3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">стойка для акустических систем черная Soundking DB009B — 2 шт.
</span>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict15">
                        <div id="titgold"><h3>Озвучивание мечети</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcom1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности 100v 120w, DSPPA MP300 - 1шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcom2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система рупорного типа Apart MPLT32G - 4шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcom3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт Behringer x1204usb - 1шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcom4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">микрофонная стойка типа "Журавль" Soundking DD007B - 1шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcom5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Речевой кардиоидный динамический микрофон SHURE PGA58 - 1шт

</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict16">
                        <div id="titgold"><h3>Озвучивание Помещений</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcoo1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">6-зонный усилитель мощности со встроенным MP3 проигрывателем и радио тюнером DSPPA MP1010U - 1шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcoo2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Динамический микрофон на подставке, с выключателем DSPPA EC200 - 1шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoo3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система 100v c динамиком 1х8 HK-Audio IL80TW - 10шт
</span>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict17">
                        <div id="titgold"><h3>Постановочное световое оборудование</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcoss1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Консоль управления световыми приборами Martin Profssional M1 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcoss2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным вращением CHAUVET_Rogue-R2 WASH — 8 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoss3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным вращением PR Lighting XR 440 BWS — 4 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoss4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор с полным вращением CHAUVET_Rogue-R2_SPOT — 8 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoss5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Театральный прожектор DTS Scena S – 20 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoss6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Прожектор направленного света DTS PAR 64 Classic — 20 шт.
</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict18">
                        <div id="titgold"><h3>Профессиональный звуковой комплект для театра</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcopr1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Элемент компактного, активного, линейного массива DB Technologies DVA M2M — 8 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcopr2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Элемент компактного, активного, линейного массива, суб бас система DB Technologies DVA M2S  — 4 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcopr3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">DSP Процессор управления акустическими системами Dynacord DSP260 — 1 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcopr4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Цифровая микшерная консоль Midas M32 — 1 шт.

</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict19">
                        <div id="titgold"><h3>Комплект звукового оборудования Премиум класса</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcoe1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активная 3х полосная акустическая система  Electro-voice ETX35P - 2шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcoe2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активный Саб Вуфер со встроенным DSP процессором Electro-Voice ETX18SP - 4шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoe3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт со сдвоенным FX процессором Dynacord CMS600 - 1шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoe4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Радио система с рчным передатчиком SHURE SLX24E/SM58 - 2шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoe5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор, проекционно-многолучевой светодиодный Chauvet Circus2.0 - 2шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoe6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">световой прибор, тонкий прожектор направленного света Chauvet Slim Par 56 — 4 шт.

</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict20">
                        <div id="titgold"><h3>Мобильный комплект звукового оборудования</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcoa1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Радиосистема с ручным передатчиком SHURE BLX24E/PG58 — 1 шт.</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcoa2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активный сабвуфер Behringer VQ1800D — 2 шт.</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoa3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активная акустическая система Behringer B215D — 2 шт.
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoa4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Микшерный пульт Behringer X1832USB — 1 шт.

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoa5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Световой прибор, проекционно-многолучевой светодиодный Chauvet Circus2.0 - 2шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoa6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">световой прибор, тонкий прожектор направленного света Chauvet Slim Par 56 — 4 шт.

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoa7">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Межколоночная стойка "Саб-Топ" Soundking DB023B — 2 шт.

</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict21">
                        <div id="titgold"><h3>Концертная площадка до 500 человек</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcoi1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Stage Box 16 входа, 8 выходов, для цифровой консоли, Behringer s16 - 1шт</span>
                                </div>

                                <div class="tooltip tooltip-east" id="pcoi3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Цифровой акустический процессор, Dynacord DSP260 - 1шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoi4">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Усилитель мощности 2x1200w, Dynacord SL2400 - 3шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoi5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Акустическая система, 1000W RMS динамики 2x15, Dynacord DC-C25.2 - 2шт
</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoi6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Цифровая Микшерная Консоль Behringer X32Compact

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoi7">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">активная акустическая система, напольный монитор Dynacord AXM12A - 4шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcoi7">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Рековая стойка с регулируемой поверхностью Soundking DF025 - 1шт

</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="pict22">
                        <div id="titgold"><h3>Озвучивание крупных концертных площадок</h3></div>
                        <div id="prgold"><h1>по запросу</h1>

                        </div>
                        <div class="dummy dummy-image">
                            <div class="dummy-pois">
                                <div class="tooltip tooltip-west" id="pcok1">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Цифровая микшерная консоль Midas M32 - 1шт</span>
                                </div>
                                <div class="tooltip tooltip-west" id="pcok2">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активная АС, элемент линейного массиваДинамики: Low: 1 х 8", Mid: 1 x 6.5'', High: 2 x 1" DB Technologies DVA K5 - 12шт</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcok3">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активный сценический напольный монитор 300W rms DB Technologies FM12 - 4шт
</span>
                                </div>

                                <div class="tooltip tooltip-east" id="pcok5">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Цифровой акустический процессор, Electro-Voice DC-One - 1шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcok6">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Активный сабвуфер c динамиками 2 х 18 DB Technologies DVA KS20 - 6шт

</span>
                                </div>
                                <div class="tooltip tooltip-east" id="pcok7">
                                    <span class="tooltip-item"></span>
                                    <span class="tooltip-content">Stage Box 32 входа, 16 выходов, для цифровой консоли, Midas DL32 1шт

</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row" id="marg"><p style="color:black; text-align:left;"><b>Проекционное оборудование</b></p>
                    <p style="color:black; text-align:left;">Проектор Важная часть многих театральных постановок. Но
                        разнообразие проекторов делает его

                        выбор очень сложным. Наши специалисты с радостью помогут подобрать Вам проекционное

                        оборудование с учетом Ваших требований.</p></br>
                    <p style="color:black; text-align:left;"><b>Театральный свет</b></p>
                    <p style="color:black; text-align:left;">Сложно представить себе театральную постановку не
                        обыгранную световыми лучами.

                        Театральное световое оборудование очень специфично, мы готовы сделать для Вас

                        индивидуальный проект, все доставить и установить.
                    </p></br>
                    <p style="color:black; text-align:left;"><b>Спецэффекты для съемок клипов</b></p>
                    <p style="color:black; text-align:left;">Музыкальные клипы порой требуют спецэффектов. Если Вы
                        занимаетесь съемками клипов, то Вы

                        наверняка сталкивались с необходимостью сделать снег летом или раздуть волосы певице. Наша

                        компания занимается продажей таких спецэффектов.</p>
                    </br>
                    <p style="color:black; text-align:left;"><b>Звуковое оборудование</b></p>
                    <p style="color:black; text-align:left;">Съемки кино и клипов, театральные постановки не могут
                        обойтись без звукового оборудования.

                        У нас Вы можете заказать выезд специалиста для просчета и замеров, мы все привезем и

                        установим, а так же оказываем пост продажную поддержку.</p>
                </div>
            </div>

        </div>
    </section>

    <section class="fomm">
        <h1 id="bbw2">Не нашли подходящий комплект?</h1>
        <div class="borders1">
            <div class="row">
                <div id="form" class="form-inline">
                    <div class="form-group">

                        <input type="text" name="username" class="form-control name1" id="exampleInputName"
                               placeholder="Имя">
                    </div>
                    <div class="form-group">

                        <input type="autofocus" name="phone" class="form-control tel1" id="exampleInputPhone"
                               placeholder="Телефон">
                    </div>
                    <button type="submit" id="sub1" class="btn btn-default nomodal send_post">Подобрать комплект
                    </button>
                </div>
            </div>
        </div>
</div>
</section>
<section class="socsety">
    <div class="border3">
        <div class="row">
            <div class="col-md-2"><img src="/wp-content/themes/twentysixteen/images/envelope.png"></div>
            <div class="col-md-2"><a href="https://www.instagram.com/realsoundkz/" target="_blank"><img
                            src="/wp-content/themes/twentysixteen/images/instagram.png"></a></div>
            <div class="col-md-2"><a href="https://www.facebook.com/REALSOUNDKZ/" target="_blank"><img
                            src="/wp-content/themes/twentysixteen/images/facebook.png"></a></div>
            <div class="col-md-2"><a href="https://vk.com/club107718739" target="_blank"><img
                            src="/wp-content/themes/twentysixteen/images/vk.png"></a></div>
            <div class="col-md-2"><a href="https://www.youtube.com/channel/UCI1jwpjBlCD4_qFLH9Go46A"
                                     target="_blank"><img
                            src="/wp-content/themes/twentysixteen/images/youtube-logo.png"></a></div>
        </div>
    </div>
</section>
<?php require_once 'map.php'; ?>
<svg id="hid" xmlns="https://www.w3.org/2000/svg" version="1.1">
    <filter id="blur">
        <feGaussianBlur stdDeviation="5"/>
    </filter>

    <filter id="blurLogo">
        <feGaussianBlur stdDeviation="1"/>
    </filter>
</svg>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/wow.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/shopinnerscript.js"></script>
<script src="/wp-content/themes/twentysixteen/js/remodal.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/shopscript.js"></script>
<script src="/wp-content/themes/twentysixteen/js/jquery.maskedinput.min.js"></script>
<script src="/wp-content/themes/twentysixteen/js/scr.js"></script>
<script src="/wp-content/themes/twentysixteen/js/forms.js"></script>
<?php require_once 'counters.php'; ?>
</body>
</html>
