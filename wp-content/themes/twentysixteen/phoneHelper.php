<?php
/**
 * Created by PhpStorm.
 * User: danilpovarov
 * Date: 29.07.2018
 * Time: 19:23
 */

$url = "http://adm.reals.kz/get-contacts";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);
$phones = json_decode($result, JSON_FORCE_OBJECT);

$phoneMain = '+7 (727) 335 65 69';
$phoneMainNormalize = '+77273356569';
$phoneMobile = '+7 (777) 000 80 11';
$phoneMobileNormalize = '+77770008011';
$showBoth = true;

if ($phones) {
    $phoneMobile = $phones['phone'];
    $phoneMobileNormalize = $phones['clearPhone'];
}

if ($phones && $phones['change']) {
    $phoneMain = $phones['phone'];
    $phoneMainNormalize = $phones['clearPhone'];
    $showBoth = false;
}

$now = new \DateTime();
$hour = (int)$now->format('H');
if ($hour >= 22) {
    $showBoth = false;
}

define('MAIN_PHONE', $phoneMain);
define('MOBILE_PHONE', $phoneMobile);
define('BOTH_PHONE', $showBoth);