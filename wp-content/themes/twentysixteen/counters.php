<?php
/**
 * Created by PhpStorm.
 * User: danilpovarov
 * Date: 29.07.2018
 * Time: 19:23
 */
?>
<script type='text/javascript'>
    (function(){ var widget_id = 's9tnUDZfIY';var d=document;var w=window;function l(){ var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130018980-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-130018980-1');
</script>

<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter28937900 = new Ya.Metrika({
                    id: 28937900,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/28937900" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>

<script type="text/javascript">
    (function (d, w, k) {
        w.introvert_callback = function () {
            try {
                w.II = new IntrovertIntegration(k);
            } catch (e) {
                console.log(e)
            }
        };

        var n = d.getElementsByTagName("script")[0],
            e = d.createElement("script");

        e.type = "text/javascript";
        e.async = true;
        e.src = "https://api.yadrocrm.ru/js/cache/" + k + ".js";
        n.parentNode.insertBefore(e, n);
    })(document, window, '8657500e');
</script>
