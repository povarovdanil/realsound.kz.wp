<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
    <section class="slider">
        <div id="fb-root"></div>
        <div id="container" class="wrapper">
            <ul id="scene" class="scene unselectable" data-friction-x="0.1" data-friction-y="0.1" data-scalar-x="25"
                data-scalar-y="15">
                <li class="layer" data-depth="0.01">
                    <div class="background"></div>
                </li>
                <li class="layer" id="dm">
                    <div class="smoke"><img src="/wp-content/themes/twentysixteen/images/smoke.png"></div>
                </li>
                <li class="layer" data-depth="0.40">
                    <ul class="rope depth-60">
                        <li class="hanger position-3">
                            <div class="board birds swing-5"></div>
                        </li>
                        <li class="hanger position-6">
                            <div class="board cloud-2 swing-2"></div>
                        </li>
                        <li class="hanger position-8">
                            <div class="board cloud-3 swing-4"></div>
                        </li>
                    </ul>
                </li>
                <li class="layer">
                    <div class="girl"><img src="/wp-content/themes/twentysixteen/images/girl.png"></div>
                </li>
                <li class="layer">
                    <div class="scen"></div>
                </li>
                <li class="layer">
                    <div class="txt1"><?php $my_post = get_post(60);
                        echo $my_post->post_content; ?></div>
                </li>
            </ul>
        </div>
    </section>
    <section id="ex3">
        <div class="service-margin"></div>
        <h2 style="text-transform:uppercase;">Узнайте о нас за <span id="threemin">3</span> минуты</h2>
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <div class="myvideo">
                    <iframe src="https://www.youtube.com/embed/4lhIoraJROQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <!--<div class="leftblock">
		<article class="post">
			<div class="txt"><?php //$my_post = get_post(58); echo $my_post->post_content;?></div>
			<div class="myvideo" style="transform: skew(-15deg);"><iframe width="560" height="315" src="https://www.youtube.com/embed/9NKsMSEIup4" frameborder="0" allowfullscreen></iframe></div>
		</article>
	</div>
	<div class="rightblock">
		<article class="post last"> <?php $my_post = get_post(56);
        echo $my_post->post_content; ?> </article>
	</div>-->
    </section>
    <section class="drums" id="drums">
        <div id="container" class="wrapper">
            <div class="service-margin"></div>
            <h2 style="color:gold; text-transform:uppercase;" id="rent">Наши услуги</h2>
            <div class="sc1"></div>
            <div class="tabs">
                <input id="tab1" type="radio" name="tabs" checked> <label for="tab1" title="Вкладка 1">Звуковое</br>
                    оборудование</label> <input id="tab2" type="radio" name="tabs"> <label for="tab2" title="Вкладка 2">Световое</br>
                    оборудование</label> <input id="tab3" type="radio" name="tabs"> <label for="tab3" title="Вкладка 3">LED
                    экраны</br>всех размеров</label> <input id="tab4" type="radio" name="tabs"> <label for="tab4"
                                                                                                       title="Вкладка 4">Ферма
                    и</br>сцена</label> <input id="tab5" type="radio" name="tabs"> <label for="tab5" title="Вкладка 4">Спецэффекты</br>
                    <span style="color:transparent">Спецэффекты</span></label> <input id="tab6" type="radio"
                                                                                      name="tabs"> <label for="tab6"
                                                                                                          title="Вкладка 4">Бэклайн</br>
                    <span style="color:transparent">Спецэффекты</span></label>
                <section id="content1">
                    <p id="oborod"> <?php $my_post = get_post(54);
                        echo $my_post->post_content; ?> </p>
                    <a href="https://realsound.kz/arenda-zvukovoe-oborudovanie/"><input id="readmore" type="button"
                                                                                       class="btn btn-info"
                                                                                       value="Подробнее"></a>
                </section>
                <section id="content2">
                    <p id="oborod"> <?php $my_post = get_post(52);
                        echo $my_post->post_content; ?> </p>
                    <a href="https://realsound.kz/arenda-svetovoe-oborudovanie/"><input id="readmore" type="button"
                                                                                       class="btn btn-info"
                                                                                       value="Подробнее"></a>
                </section>
                <section id="content3">
                    <p id="oborod"> <?php $my_post = get_post(50);
                        echo $my_post->post_content; ?> </p>
                    <a href="https://realsound.kz/arenda-led-oborudovanie/"><input id="readmore" type="button"
                                                                                  class="btn btn-info"
                                                                                  value="Подробнее"></a>
                </section>
                <section id="content4">
                    <p id="oborod"> <?php $my_post = get_post(48);
                        echo $my_post->post_content; ?> </p>
                    <a href="https://realsound.kz/arenda-ferma-scena/"><input id="readmore" type="button"
                                                                             class="btn btn-info" value="Подробнее"></a>
                </section>
                <section id="content5">
                    <p id="oborod"> <?php $my_post = get_post(45);
                        echo $my_post->post_content; ?> </p>
                    <a href="https://realsound.kz/arenda-speceffety/"><input id="readmore" type="button"
                                                                            class="btn btn-info" value="Подробнее"></a>
                </section>
                <section id="content6">
                    <p id="oborod"> <?php $my_post = get_post(43);
                        echo $my_post->post_content; ?> </p>
                    <a href="https://realsound.kz/arenda-backline/"><input id="readmore" type="button"
                                                                          class="btn btn-info" value="Подробнее"></a>
                </section>
            </div>
        </div>
    </section>
    <section class="direction">
        <h2 style="color:black; text-transform:uppercase;margin-bottom: 4%;">Направления услуг</h2>
        <div id="blackarr" class="navigat_service">
            <div>
                <div class="col-md-4" id="wdt">
                    <a href="https://realsound.kz/arenda-meropriyatiya/">
                        <div id="usluga1" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon1"></div>
                                <h3 id="txtu">Активно развлекательные мероприятия</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3" id="wdt1">
                    <a href="https://realsound.kz/arenda-vypusknoi/">
                        <div id="usluga2" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon2"></div>
                                <h3 id="txtu">Выпускные</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-goverment/">
                        <div id="usluga3" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon3"></div>
                                <h3 id="txtu">Государственные мероприятия</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-dni-rozhdeniya/">
                        <div id="usluga4" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon4"></div>
                                <h3 id="txtu">Дни рождения</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div>
                <div class="col-md-4" id="wdt">
                    <a href="https://realsound.kz/arenda-konferencii/">
                        <div id="usluga5" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon5"></div>
                                <h3 id="txtu">Конференции</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-koncerty/">
                        <div id="usluga6" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon6"></div>
                                <h3 id="txtu">Концерты</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-korporativy/">
                        <div id="usluga7" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon7"></div>
                                <h3 id="txtu">Корпоративы</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-school/">
                        <div id="usluga8" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon8"></div>
                                <h3 id="txtu">Образовательные мероприятия</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div>
                <div class="col-md-4" id="wdt">
                    <a href="https://realsound.kz/arenda-openair/">
                        <div id="usluga9" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon9"></div>
                                <h3 id="txtu">Open Air</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-pr/">
                        <div id="usluga10" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon10"></div>
                                <h3 id="txtu">PR акции</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-wedding/">
                        <div id="usluga11" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon11"></div>
                                <h3 id="txtu">Cвадьбы</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-seminary/">
                        <div id="usluga12" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon12"></div>
                                <h3 id="txtu">Cеминары</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div>
                <div class="col-md-4" id="wdt">
                    <a href="https://realsound.kz/arenda-sport/">
                        <div id="usluga13" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon13"></div>
                                <h3 id="txtu">Cпортивные мероприятия</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-teambuilding/">
                        <div id="usluga14" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon14"></div>
                                <h3 id="txtu">Тимбилдинги</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-fashion/">
                        <div id="usluga15" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon15"></div>
                                <h3 id="txtu">Фэшн показы</h3>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" id="wdt1">
                    <a href="https://realsound.kz/arenda-nayubilei/">
                        <div id="usluga16" class="hvr-border-fade">
                            <div id="yell">
                                <div id="icon16"></div>
                                <h3 id="txtu">Юбилеи</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="banner">
        <div class="sale">
            <div id="border">
                <h2 id="vb">СКИДКА 20%</h2>
                <h4>При бронировании за</br>2 месяца вперед</h4>
            </div>
            <a href="#modal"><input type="submit" class="btn btn-info" value="ЗАКАЗАТЬ ЗВОНОК"></a>
            <div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title"
                 aria-describedby="modal1Desc">
                <div class="remodalBorder">
                    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                    <div id="form">
                        <h2 id="modal1Title">Оставьте ваши контактные данные и наш консультант свяжется с вами</h2>
                        <input type="text" class="putName" name="name" placeholder="Ваше имя" id="exampleInputName"
                               required> <input name="phone" id="exampleInputPhone" type="autofocus" class="putPhone"
                                                placeholder="Ваш номер телефона" required> <input type="submit"
                                                                                                  name="submit"
                                                                                                  class="send_post"
                                                                                                  id="btnw"
                                                                                                  value="ОТПРАВИТЬ">
                    </div>
                </div>
            </div>
            <div class="remodal" data-remodal-id="modal2" role="dialog" aria-labelledby="modal1Title"
                 aria-describedby="modal1Desc">
                <div class="remodalBorder">
                    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                    <div id="form" class="form_resume">
                        <h2 id="modal1Title">Оставьте ваши контактные данные</h2>
                        <input type="text" class="putName" name="name" placeholder="Ваше имя" id="exampleInputName"
                               required> <input name="phone" id="exampleInputPhone" type="autofocus" class="putPhone"
                                                placeholder="Ваш номер телефона" required> <input type="file"
                                                                                                  name="file"
                                                                                                  id="exampleInputFile">
                        <input type="submit" name="submit" class="send_resume" id="btnw" value="ОТПРАВИТЬ РЕЗЮМЕ">
                    </div>
                </div>
            </div>
        </div>
        <div class="picsale"><img src="/wp-content/themes/twentysixteen/images/calendar.jpg"></div>
    </section>
    <section class="gal">
        <a href="#gallery" id="gallery">
            <h2 style="color:white; text-transform:uppercase;">Галерея</h2>
        </a>
        <div class="sliderr responsive">
            <div id="one"><?php $my_post = get_post(41);
                echo $my_post->post_content; ?></div>
            <div id="two"><?php $my_post = get_post(39);
                echo $my_post->post_content; ?></div>
            <div id="three"><?php $my_post = get_post(37);
                echo $my_post->post_content; ?></div>
            <div id="four"><?php $my_post = get_post(235);
                echo $my_post->post_content; ?></div>
            <div id="five"><?php $my_post = get_post(237);
                echo $my_post->post_content; ?></div>
            <div id="six"><?php $my_post = get_post(239);
                echo $my_post->post_content; ?></div>
            <div id="seven"><?php $my_post = get_post(241);
                echo $my_post->post_content; ?></div>
            <div id="eight"><?php $my_post = get_post(243);
                echo $my_post->post_content; ?></div>
        </div>
    </section>
    <section class="dj">
        <h2 id="byel">Ищем таланты</h2>
        </br>
        <h2 id="yel">Ищите работу?</h2>
        <h2 id="yel">А мы как раз ищем Вас!<span style="color:white;"> Если Вы:</span></h2>
        <div class="row">
            <div class="col-md-6">
                <div class="blacktr"><?php $my_post = get_post(35);
                    echo $my_post->post_content; ?> </div>
            </div>
            <div class="col-md-6">
                <div class="blacktr"><?php $my_post = get_post(31);
                    echo $my_post->post_content; ?></div>
            </div>
        </div>
        </br>
        <div class="row">
            <div class="col-md-6">
                <div class="blacktr"><?php $my_post = get_post(33);
                    echo $my_post->post_content; ?></div>
            </div>
            <div class="col-md-6">
                <div class="blacktr"><?php $my_post = get_post(29);
                    echo $my_post->post_content; ?></div>
            </div>
        </div>
        <a href="#modal2"><input type="submit" class="btn btn-info" value="ОТПРАВИТЬ РЕЗЮМЕ"></a>
    </section>
    <section class="decision">
        <h2 id="bb">ГОТОВЫЕ РЕШЕНИЯ</h2>
        <div class="row">
            <a href="/готовые-решения">
                <div class="col-md-4" id="square1">
                    <h2 id="pocket">MEDIUM</h2>
                    <div class="caption fade-caption">
                        <div class="col-md-6 img-container"><img src="/wp-content/uploads/2016/11/med.png"></div>
                        <div class="col-md-6"><?php $my_post = get_post(27);
                            echo $my_post->post_content; ?></div>
                    </div>
                </div>
            </a>
            <a href="/готовые-решения">
                <div class="col-md-4" id="square2">
                    <h2 id="pocket">LARGE</h2>
                    <div class="caption fade-caption">
                        <div class="col-md-6 img-container"><img src="/wp-content/uploads/2016/11/large.png"></div>
                        <div class="col-md-6"><?php $my_post = get_post(25);
                            echo $my_post->post_content; ?></div>
                    </div>
                </div>
            </a>
            <a href="/готовые-решения">
                <div class="col-md-4" id="square3">
                    <h2 id="pocket">XTRALARGE</h2>
                    <div class="caption fade-caption">
                        <div class="col-md-6 img-container"><img src="/wp-content/uploads/2016/11/xlarge.png"></div>
                        <div class="col-md-6"><?php $my_post = get_post(21);
                            echo $my_post->post_content; ?></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="row" id="pics">
            <h2 id="bbw">Не нашли</br> подходящего оборудования?</h2>
            <a href="#modal"><input type="submit" class="btn btn-info" value="ЗАКАЗАТЬ ЗВОНОК"></a>
        </div>
    </section>
    <?php require_once 'map.php'; ?>
    </div>
<?php get_footer(); ?>