<?php 

$data = array();
 
if( isset( $_GET['uploadfiles'] ) ){
    $error = false;
    $files = array();
 
    $uploaddir = './uploads_resume/';
 
    if(!is_dir($uploaddir)) mkdir($uploaddir, 0777);

    $path = $_FILES[0]['name'];
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    $name = uniqid();
    $name_file = $name.'.'.$ext;
 
    if(move_uploaded_file($_FILES[0]['tmp_name'], $uploaddir . $name_file)){
        $file_href = "https://realsound.kz/uploads_resume/". $name_file;
    }
    else{
        $error = true;
    }

    if ($error){
		$result = array(
		    'status' => 'error',
		    'message' => 'Ошибка, сообщение не отправлено, попробуйте еще раз.'
		);
    	echo json_encode($result);
    } else {
    	$address = "info@realsound.kz";
		$mes = '
				<html>
					<head>
					  <title></title>
					</head>
					<body>
					  	<p><b>Имя: </b> '.$_REQUEST['username'].'</p>
						<p><b>Телефон: </b> '.$_REQUEST['phone'].'</p>
						<p><a href="'.$file_href.'" download>Скачать резюме</a></p>
					</body>
				</html>
				';

		/* А эта функция как раз занимается отправкой письма на указанный вами email */
		$sub = 'Отправлено резюме с сайта realsound.kz'; 
		$email = 'info@realsound.kz'; // от кого
		if(mail ($address,$sub,$mes,"Content-type:text/html; charset = utf-8\r\nFrom:$email")){
			$result = array(
			    'status' => 'ok',
			    'message' => 'Спасибо, ваше сообщение отправлено.'
			);
			echo json_encode($result);
		}
    }

}

?>